import csv

TOXIC_TRAIN = 'data/toxic_comments/train.csv'
TOXIC_TEST = 'data/toxic_comments/test.csv'
INSULT_TRAIN = 'data/insult_detection/train.csv'

class Data():
    def __init__(self, header, data):
        self.header = header
        self.data = data

    def filter(self, func):
        return [r for r in self.data if func(r)]

def bool_from_num_string(s):
    return s.strip() == '1'

def apply_transforms(row, transforms):
    for i in range(len(transforms)):
        if transforms[i] != None:
            row[i] = transforms[i](row[i])
    return row

def clean(s):
    rem = '".!\'<>/;:[]{}`~|\\-=_+·1234567890'
    for r in rem:
        s = s.replace(r, '')
    return s.lower().strip().split()

def load(fname, transforms=[]):
    # read data
    with open(fname, 'r') as f:
        reader = csv.reader(f)
        header = None
        data = []
        line = 0
        for row in reader:
            if header == None:
                header = row
            else:
                if len(row) == len(header):
                    row = apply_transforms(row, transforms)
                    data.append(row)
                else:
                    print('ERROR: Invalid row length on line', line)
            line += 1
    return Data(header, data)

def toxic_filter(r):
    if not r[3]:
        return False
    forbidden = [
        'wikipedia', 'edits', 'block', 'unblock', 'blocking', 'wiki',
        'post', 'posts',
    ]
    for f in forbidden:
        if f in r[1]:
            return False
    return True

def load_toxic_comments():
    return load(TOXIC_TRAIN, [
        None,
        clean,
        bool_from_num_string,
        bool_from_num_string,
        bool_from_num_string,
        bool_from_num_string,
        bool_from_num_string,
        bool_from_num_string,
    ])

def load_insults():
    return load(INSULT_TRAIN, [
        bool_from_num_string,
        None,
        clean,
    ])