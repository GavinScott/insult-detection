import data_loader
import markov

if __name__ == '__main__':
    insults = data_loader.load_insults()
    toxic = data_loader.load_toxic_comments()
    content = []
    content = [r[2] for r in insults.filter(lambda r: r[0])]
    content.extend([r[1] for r in toxic.filter(data_loader.toxic_filter)])
    m = markov.Markov(content)
    out = ' '.join(m.generate(n=3))
    print('{}\n{}'.format('-'*70, out, '-'*70))