import numpy as np

class End():
    def __init__(self):
        pass
    def __str__(self):
        return ''

class Markov():
    def __init__(self, src):
        print(len(src))
        self.src = [r + [End()] for r in src]
        self.starts = {}
        for sent in self.src:
            if sent[0] not in self.starts:
                self.starts[sent[0]] = 0
            self.starts[sent[0]] += 1

    def __has_sublist(self, li, sub):
        for ndx in range(len(li) - len(sub) + 1):
            if li[ndx:ndx + len(sub)] == sub:
                return ndx
        return -1

    def __get_word(self, gram):
        if len(gram) == 0:
            counts = self.starts
            sources = len(counts)
        else:
            counts = {}
            sources = 0
            for row in self.src:
                ndx = self.__has_sublist(row, gram)
                if ndx >= 0:
                    sources += 1
                    word = row[ndx + len(gram)]
                    if word not in counts:
                        counts[word] = 0
                    counts[word] += 1
        # probs
        words = list(counts.keys())
        length = sum(counts.values())
        probs = [counts[w] / length for w in words]
        print('Sources:', sources)
        return np.random.choice(words, p=probs), sources

    def __generate(self, n, max_words, min_avg_srcs, outlier_thresh):
        output = []
        all_sources = []
        while len(output) == 0 or type(output[-1]) != End:
            word, sources = self.__get_word(output[-n:])
            output.append(word)
            all_sources.append(sources if sources <= outlier_thresh
                else outlier_thresh)
            if max_words != None and len(output) > max_words:
                print('Ending early, max_words reached')
                break
        print('All:', all_sources)
        avg = min_avg_srcs
        if len(all_sources) > 0:
            avg = sum(all_sources) / len(all_sources)
        return output[:-1], avg

    def generate(self, n=1, max_words=75, min_length=4, min_avg_srcs=3,
            outlier_thresh=10, max_tries=10):
        avg_sources = min_avg_srcs - 1
        output = []
        tries = 0
        best = (None, None)
        while avg_sources < min_avg_srcs or len(output) < min_length:
            tries += 1
            print('Generating...\n')
            output, avg_sources = self.__generate(n, max_words,
                min_avg_srcs, outlier_thresh)
            print('AVG:', avg_sources, '\n')
            if best[0] == None or avg_sources > best[1]:
                best = (output, avg_sources)
            if tries >= max_tries:
                print('Hit max attempts')
                return best[0]
        return output
